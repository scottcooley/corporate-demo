package com.deque;

import com.deque.attest.AttestConfiguration;
import com.deque.attest.AttestDriver;
import com.deque.attest.reporter.AttestReportingOptions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.deque.attest.matchers.IsAuditedForAccessibility.isAuditedForAccessibility;
import static org.hamcrest.MatcherAssert.assertThat;

public class AppTest {

    AttestDriver page;
    private WebDriver webDriver;

    @Before
    public void setUp() throws Exception {
        AttestConfiguration.configure()
                .testSuiteName("ExamplePage")
                .userAgent("Chrome")
                .testMachine("Dev Machine")
                .outputDirectory("target/attest-reports");

        System.setProperty("webdriver.chrome.driver", "C:/chromedriver/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("disable-gpu");
        webDriver = new ChromeDriver(options);
        page = new AttestDriver(webDriver);
        webDriver.get("file:///C:/corporate-demo/prod/index.html");
    }

    @After
    public void tearDown() throws Exception {
        webDriver.quit();
    }

    @Test
    public void itShouldAuditPageForAccessibility() throws Exception {
        assertThat(page, isAuditedForAccessibility().logResults(new AttestReportingOptions()
                .uiState("loading-example-page")
        ));
    }
}
